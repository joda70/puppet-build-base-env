# Based on the centos latest stable image
FROM centos:centos6
MAINTAINER Elisabetta Ronchieri <elisabetta.ronchieri@cnaf.infn.it>

# Install packages to develop code
ADD puppet_build_base_env_centos.sh /root/puppet_build_base_env.sh
RUN /root/puppet_build_base_env.sh 6

RUN yum clean all
