#!/bin/bash

OS_VERSION=$1

echo "Updating system..."
yum update -y

echo "Adding required packages..."
rpm -ivh http://dl.fedoraproject.org/pub/epel/$OS_VERSION/x86_64/epel-release-$OS_VERSION-8.noarch.rpm
rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-$OS_VERSION.noarch.rpm
yum install puppet git vim hostname openssh-server passwd rubygems ruby-devel wget redhat-lsb -y

echo "Installing puppet modules..."
puppet module install --force maestrodev-wget
puppet module install --force gini-archive
puppet module install --force puppetlabs-stdlib
puppet module install --force maestrodev-maven
puppet module install --force puppetlabs-java
